/**
 * Automatic plant waterer
 * 
 * Adapted from Instructabes tutorial by osmithy
 * 
 * https://www.instructables.com/id/Automatically-water-your-small-indoor-plant-using-/
 * 
 */

int motorPin = A0; // pin that turns on the motor
int blinkPin = 13; // pin that turns on the LED
int watertime = 10; // how long to water in seconds
int waittime = 24; // how long to wait between watering, in hours

void setup()
{
  pinMode(motorPin, OUTPUT); // set A0 to an output so we can use it to turn on the transistor
  pinMode(blinkPin, OUTPUT); // set pin 13 to an output so we can use it to turn on the LED
}

void loop()
{
  digitalWrite(motorPin, HIGH); // turn on the motor
  digitalWrite(blinkPin, HIGH); // turn on the LED
  delay(watertime*1000);        // multiply by 1000 to translate seconds to milliseconds

  digitalWrite(motorPin, LOW);  // turn off the motor
  digitalWrite(blinkPin, LOW);  // turn off the LED
  delay(waittime*1200000);        // multiply by 1200000 to translate hours to milliseconds
}
