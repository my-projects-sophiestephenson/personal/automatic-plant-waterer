## Automatic plant waterer for winter break

I created this plant waterer using an Adafruit Metro and [this Instructables tutorial](https://www.instructables.com/id/Automatically-water-your-small-indoor-plant-using-/).
It waters my indoor plant for 10 seconds every 24 hours (it doesn't need much water) 
so that the plant doesn't die while I'm home for 5 weeks over winter break.